/* RF24SensorNet ThreeNodeExample_BaseNode
   Uses an nRF24L01 radio module on pins 9 and 10,
   and does nothing except forwarding messages in the network.

   -- 
   Based on example by
   Peter Hardy https://bitbucket.org/pjhardy/rf24sensornet
   --
   
   Sept 2014, Espen Lodden
*/
#include <SPI.h>

#include <RF24.h>
#include <RF24Network.h>
#include <RF24SensorNet.h>

static uint16_t myAddr = 011; // RF24Network address of this node.


RF24 radio(9, 10);
RF24Network network(radio);
RF24SensorNet sensornet(network);

// Callback when a temperature is received.
void receiveTemperature(uint16_t fromAddr, uint16_t id, int16_t temp) {
    Serial.print("Received temperature: ");
    Serial.print((float)temp/10);
    Serial.print("\t\t");
}

// Callback when a humidity is received.
void receiveHumidity(uint16_t fromAddr, uint16_t id, uint16_t humid) {
    Serial.print("Received humidity: ");
    Serial.println((float)humid/10);
}


void setup() {
  // Initialise the SPI bus.
  SPI.begin();
  // Initialise the nRF24L01 radio.
  radio.begin();
  // Initialise the RF24Network.
  network.begin( /*channel*/ 90, /*node address*/ myAddr);
  // Initialise the RF24SensorNet
  sensornet.begin();

  // Attach a function to the Switch callback
  //sensornet.addTempRcvHandler(receiveTemperature);
  //sensornet.addHumidRcvHandler(receiveHumidity);


  
  // Initialise Serial debugging
  Serial.begin(115200);
}

void loop() {
  // Update the RF24SensorNet network
  sensornet.update();
}
