/* RF24SensorNet ThreeNodeExample_BaseNode
   Uses an nRF24L01 radio module on pins 9 and 10.
   Reads temperature and humidity data from a DHT22 sensor on pin 6
   and sends this via parent node to the root node.

   -- 
   Based on example by
   Peter Hardy https://bitbucket.org/pjhardy/rf24sensornet
   
   
   Sept 2014, Espen Lodden
*/
#include <SPI.h>

#include <RF24.h>
#include <RF24Network.h>
#include <RF24SensorNet.h>

#include <DHT.h>

// DHT22 sensor
dht DHT;
int16_t   temperature;
uint16_t  humidity;


#define DHT22_PIN 6
//

// Sensornet
static uint16_t myAddr = 0111; // RF24Network address of this node.
static uint16_t remoteAddr = 00; // RF24Network address of destination node.

int ledState = LOW; // The currrent state of the remote LED
unsigned long previousMillis = 0; // The last time the remote LED was updated
static long interval = 1000; // How often to update the LED

RF24 radio(9, 10);
RF24Network network(radio);
RF24SensorNet sensornet(network);

// Sensornet
// Callback when a Temperature status packet is received.
void getTempStatus(uint16_t fromAddr, uint16_t id, int16_t temp) {
  if (fromAddr == remoteAddr ) {
    Serial.print("Temperature was successfully sent: ");
    Serial.println(temp);
  }
}

// Callback when a Humidity status packet is received.
void getHumidStatus(uint16_t fromAddr, uint16_t id, uint16_t humid) {
  if (fromAddr == remoteAddr ) {
    Serial.print("Humidity was successfully sent: ");
    Serial.println(humid);
  }
}

// Setup
void setup() {
  // Initialise the SPI bus.
  SPI.begin();
  // Initialise the nrf24L01 radio.
  radio.begin();
  // Initialise the RF24Network.
  network.begin( /*channel*/ 90, /*node address*/ myAddr);
  // Initialise the RF24SensorNet
  sensornet.begin();

  // Attach the switch get handler callback to listen for responses.
  sensornet.addTempRcvHandler(getTempStatus);
  sensornet.addHumidRcvHandler(getHumidStatus);

  // Initialise Serial debugging
  Serial.begin(115200);
  
  Serial.println("DHT TX NODE ");
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT_LIB_VERSION);
  Serial.println();
}


// Read data from DHT22 sensor
void readDHTData(void){
  // READ DATA
  Serial.print("DHT22, \t");
  int chk = DHT.read22(DHT22_PIN);
  switch (chk)
  {
    case DHTLIB_OK:  
		Serial.print("OK,\t"); 
		break;
    case DHTLIB_ERROR_CHECKSUM: 
		Serial.print("Checksum error,\t"); 
		break;
    case DHTLIB_ERROR_TIMEOUT: 
		Serial.print("Time out error,\t"); 
		break;
    default: 
		Serial.print("Unknown error,\t"); 
		break;
  }
  
}

void loop() {
  // Pump the RF24SensorNet
  sensornet.update();

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;

    readDHTData();
    humidity = DHT.humidity*10;
    temperature = DHT.temperature*10;
    
    Serial.println("Sending temperature and humidity:");
    Serial.print("Temperature = "); Serial.println(DHT.temperature);
    sensornet.sendTemp(remoteAddr, myAddr, temperature);
    Serial.print("Humidity = "); Serial.println(DHT.humidity);
    sensornet.sendHumid(remoteAddr, myAddr, humidity);

  }
}

